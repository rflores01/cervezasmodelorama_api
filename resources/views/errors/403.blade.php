<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modelo</title>

    <link rel="icon" href="https://arvolution.com/wp-content/uploads/2019/03/grupo_modelo_logo-e1553570073779.png">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">
</head>
<body>

    <div class="ui grid" style="height: 100%;">
        <div class="three wide column"></div>
        <div class="ten wide column" style="display: flex; justify-content: center; align-items: center;">
            <div class="ui card" style="margin: 0 auto;">
                <div class="image" style="padding: 5em; background: white;">
                    <img src="https://iconape.com/wp-content/files/ve/239355/svg/239355.svg" alt="">
                </div>
                <div class="content">
                    <div style="display: flex; justify-content: space-between; align-items: center;">
                        <h2 style="margin: 0;">Error 403</h2>
                        <i class="exclamation triangle red icon" style="font-size: 1.5em;"></i>
                    </div>
                    <br>
                    <p>Lo sentimos, acceso denegado.</p>
                </div>
            </div>
        </div>
        <div class="three wide column"></div>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.js"></script>
</body>
</html>