<?php

use Illuminate\Support\Str;
use App\Http\Controllers\ModelosController;

Route::get('get-api-token', function() {
    return Str::random(200).time();
});

Route::group(['middleware' => 'controlAccess'], function() {

    /** GET */
    Route::get('orders/{id}',[ModelosController::class,'getOrders']);
    Route::get('deliveryStatus',[ModelosController::class,'deliveryStatus']);
    Route::get('ordersFeed',[ModelosController::class,'ordersFeed']);
    Route::get('clearFile',[ModelosController::class,'clearFile']);
    Route::get('clearFile2',[ModelosController::class,'clearFile2']);
    Route::get('getProduce',[ModelosController::class,'getProduce']);
    Route::get('getDelivery',[ModelosController::class,'getDelivery']);
    Route::get('affectedOrder/{id}/{delivery}',[ModelosController::class,'affectedOrder']);
    
    /** POST */
    Route::post('saveOrder',[ModelosController::class,'saveOrder']);

});