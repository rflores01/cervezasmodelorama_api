<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;

use Closure;

class ControlAccess {

    public function handle(Request $request, Closure $next)
    {
        if(!$request->header('api-token'))
        {
            return response(view('errors.api_required'));
        }

        return $next($request);
    }

}