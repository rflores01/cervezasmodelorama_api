<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use DB;
use Illuminate\Http\Request;

class ModelosController extends BaseController {

    public function getProduce() {
        try
        {
            $result = DB::table('produce')->where('status',1)->get();

            return response()->json([
                'data' => $result,
                'action' => true,
                'msg' => 'done!'
            ]);
        }
        catch(\Eception $e)
        {
            return response()->json([
                'data' => [],
                'action' => false,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function affectedOrder($id,$delivery) {
        try
        {
            $result = DB::table('orders')->where('id',$id)->update([
                'delivery' => $delivery
            ]);

            $data = DB::select('call pweb_get_orders ('.$id.');');

            $arr = [];
            foreach($data as $key => $row) {
                if($row->id == $id)
                {
                    array_push($arr,$row);
                }
            }

            $fp = fopen('C:\xampp\htdocs\cervezasmodelorama-order-event.php', 'w');
            /** PHP START */
            fwrite($fp,'<?php'.PHP_EOL.PHP_EOL);
            
            /** HEADER */
            fwrite($fp, 'header("Content-Type: text/event-stream");'.PHP_EOL);
            fwrite($fp, 'header("Access-Control-Allow-Origin: *");'.PHP_EOL);
            fwrite($fp, 'header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");'.PHP_EOL);
            fwrite($fp, 'header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");'.PHP_EOL.PHP_EOL);

            /** BODY */
            fwrite($fp, 'echo "event: message\n";'.PHP_EOL);
            fwrite($fp, "echo 'data: ".json_encode($arr[0])." ';".PHP_EOL);
            fwrite($fp, 'echo "\n\n";'.PHP_EOL.PHP_EOL);
            
            /** SSE */
            fwrite($fp, 'ob_flush();'.PHP_EOL);
            fwrite($fp, 'flush();'.PHP_EOL);
            fwrite($fp, 'sleep(1);'.PHP_EOL.PHP_EOL);

            /** PHP END */
            fwrite($fp,'?>');

            fclose($fp);

            return response()->json([
                'data' => $result,
                'action' => true,
                'msg' => 'done!'
            ]);
        }
        catch(\Eception $e)
        {
            return response()->json([
                'data' => [],
                'action' => false,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function getDelivery() {
        try
        {
            $result = DB::table('delivery')->where('status',1)->get();

            return response()->json([
                'data' => $result,
                'action' => true,
                'msg' => 'done!'
            ]);
        }
        catch(\Eception $e)
        {
            return response()->json([
                'data' => [],
                'action' => false,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function getOrders($id) {

        try
        {
            $result = DB::select('call pweb_get_orders ('.$id.');');

            return response()->json([
                'msg' => 'orders',
                'data' => $result,
                'action' => true
            ]);
        }
        catch(\Exception $e)
        {
            return response()->json([
                'msg' => $e->getMessage(),
                'data' => [],
                'action' => false
            ]);
        }

    }

    public function deliveryStatus() {

        return response()->json([
            'msg' => 'deliveryStatus',
            'data' => [1,2,3],
            'action' => true
        ]);

    }

    public function ordersFeed() {

        return response()->json([
            'msg' => 'ordersFeed',
            'data' => [1,2,3],
            'action' => true
        ]);

    }

    public function saveOrder(Request $request) {

        $data = json_decode($request->input('product'));

        try
        {
            /** INSERT IN TABLE */
            $result = DB::select('CALL pweb_save_order ('.$data->id.','.$data->user_id.','.$data->total.','.$data->amount.');');


            $fp = fopen('C:\xampp\htdocs\cervezasmodelorama-event.php', 'w');
            /** PHP START */
            fwrite($fp,'<?php'.PHP_EOL.PHP_EOL);
            
            /** HEADER */
            fwrite($fp, 'header("Content-Type: text/event-stream");'.PHP_EOL);
            fwrite($fp, 'header("Access-Control-Allow-Origin: *");'.PHP_EOL);
            fwrite($fp, 'header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");'.PHP_EOL);
            fwrite($fp, 'header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");'.PHP_EOL.PHP_EOL);

            /** BODY */
            fwrite($fp, 'echo "event: message\n";'.PHP_EOL);
            fwrite($fp, "echo 'data: ".json_encode($data)." ';".PHP_EOL);
            fwrite($fp, 'echo "\n\n";'.PHP_EOL.PHP_EOL);
            
            /** SSE */
            fwrite($fp, 'ob_flush();'.PHP_EOL);
            fwrite($fp, 'flush();'.PHP_EOL);
            fwrite($fp, 'sleep(1);'.PHP_EOL.PHP_EOL);

            /** PHP END */
            fwrite($fp,'?>');

            fclose($fp);

            return response()->json([
                'action' => true,
                'msg' => 'done!!',
                'data' => $result
            ]);
        }
        catch(\Exception $e)
        {
            return response()->json([
                'action' => false,
                'msg' => $e->getMessage(),
                'data' => []
            ]);
        }

    }

    public function clearFile() {
        try
        {
            $fp = fopen('C:\xampp\htdocs\cervezasmodelorama-event.php', 'w');
            /** PHP START */
            fwrite($fp,'<?php'.PHP_EOL.PHP_EOL);
            
            /** HEADER */
            fwrite($fp, 'header("Content-Type: text/event-stream");'.PHP_EOL);
            fwrite($fp, 'header("Access-Control-Allow-Origin: *");'.PHP_EOL);
            fwrite($fp, 'header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");'.PHP_EOL);
            fwrite($fp, 'header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");'.PHP_EOL.PHP_EOL);

            /** BODY */
            // fwrite($fp, 'echo "event: message\n";'.PHP_EOL);
            // fwrite($fp, "echo 'data: {".'"orderid"'.": 1, ".'"customerid"'.": 5140 }';".PHP_EOL);
            // fwrite($fp, 'echo "\n\n";'.PHP_EOL.PHP_EOL);
            
            /** SSE */
            fwrite($fp, 'ob_flush();'.PHP_EOL);
            fwrite($fp, 'flush();'.PHP_EOL);
            fwrite($fp, 'sleep(1);'.PHP_EOL.PHP_EOL);

            /** PHP END */
            fwrite($fp,'?>');

            fclose($fp);

            return response()->json([
                'action' => true,
                'msg' => 'done!!',
                'data' => []
            ]);
        }
        catch(\Exception $e)
        {
            return response()->json([
                'action' => false,
                'msg' => $e->getMessage(),
                'data' => []
            ]);
        }       
    }
    
    public function clearFile2() {
        try
        {
            $fp = fopen('C:\xampp\htdocs\cervezasmodelorama-order-event.php', 'w');
            /** PHP START */
            fwrite($fp,'<?php'.PHP_EOL.PHP_EOL);
            
            /** HEADER */
            fwrite($fp, 'header("Content-Type: text/event-stream");'.PHP_EOL);
            fwrite($fp, 'header("Access-Control-Allow-Origin: *");'.PHP_EOL);
            fwrite($fp, 'header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");'.PHP_EOL);
            fwrite($fp, 'header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");'.PHP_EOL.PHP_EOL);

            /** BODY */
            // fwrite($fp, 'echo "event: message\n";'.PHP_EOL);
            // fwrite($fp, "echo 'data: {".'"orderid"'.": 1, ".'"customerid"'.": 5140 }';".PHP_EOL);
            // fwrite($fp, 'echo "\n\n";'.PHP_EOL.PHP_EOL);
            
            /** SSE */
            fwrite($fp, 'ob_flush();'.PHP_EOL);
            fwrite($fp, 'flush();'.PHP_EOL);
            fwrite($fp, 'sleep(1);'.PHP_EOL.PHP_EOL);

            /** PHP END */
            fwrite($fp,'?>');

            fclose($fp);

            return response()->json([
                'action' => true,
                'msg' => 'done!!',
                'data' => []
            ]);
        }
        catch(\Exception $e)
        {
            return response()->json([
                'action' => false,
                'msg' => $e->getMessage(),
                'data' => []
            ]);
        }       
    } 

}